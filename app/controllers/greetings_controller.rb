class GreetingsController < ApplicationController
  def index
    render(:index, locals: { user_description: user_description })
  end

  private

  def user_description
    user && [user.class, user.email].join(" ") || "Silly goose"
  end

  def user
    current_user || current_punter
  end
end
