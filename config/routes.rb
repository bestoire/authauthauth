Rails.application.routes.draw do
  devise_for :punters
  devise_for :users

  get "/" => "greetings#index"
end
